import random

def create_dict():
    #create variables
    fam = {}
    tally = 1
    l = []
    #loop lets user add only as many names as they want to add
    while "n" not in l:
        fam[tally] = input('Name of person in gift exchange (enter \'n\' if done): ')
        l.append(fam[tally])
        tally += 1
    #removes the key-entry pair "n" from the dictionary
    fam.popitem()
    #formatting
    print("")
    return fam


def make_pairs(fam):
    #creating variables
    has_santa_target = []
    has_santa = []
    random_family = fam.get(random.randint(1,len(fam)))
    #loop to create random list of gift givers
    for i in range(len(fam)):
        #checks to make sure no duplicates are being added
        while random_family in has_santa_target:
            random_family = fam.get(random.randint(1,len(fam)))
        has_santa_target.append(random_family)
    #loop to create random list of gift receivers
    for i in range(len(fam)):
        #loop to ensure no duplicates are added
        while random_family in has_santa:
            random_family = fam.get(random.randint(1,len(fam)))
        has_santa.append(random_family)
    #if someone is gifting to themselves by random chance, this moves their
    #-  name to the end of the list, and shifts the rest of the list forwards
    #-- to correct the error
    for i in range(len(fam)):
        if has_santa_target[i] == has_santa[i]:
            has_santa.remove(has_santa[i])
            has_santa.append(has_santa[i])
    return [has_santa_target,has_santa]

def present_combos(gift_givers, gift_receivers, fam):
    #creating the one variable
    str_list = []
    #loop puts all of the strings to print into a list
    for i in range(len(fam)):
        a = gift_receivers[i] + " ----> " + gift_givers[i]
        str_list.append(a)
    #strings are all printed with the list that was just created
    for i in str_list:
        print(i,end="\n\n")

def main():
    #creating variables
    cont = 'y'
    #the bulk of the application running by calling the various functions
    while cont == 'y':
        names = create_dict()
        question = 'n'
        while question == 'n':
            #simple error handling for the one major error i'm anticipating
            try:
                new_lists = make_pairs(names)
            except IndexError:
                print("Enter more than one name... get it together man...")
            try:
                present_combos(new_lists[0],new_lists[1], names)
            except UnboundLocalError:
                print("Program will restart now.")
            question = input("Enter 'n' to re-roll with those names. If satisfied, enter 'y' (to change the names continue): ")
        cont = input("Enter 'y' to start over with new names: ")
    print("Happy holidays!")


if __name__ == "__main__":
    main()
